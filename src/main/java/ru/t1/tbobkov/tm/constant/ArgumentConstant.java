package ru.t1.tbobkov.tm.constant;

public final class ArgumentConstant {

    private ArgumentConstant() {
    }

    public static final String HELP = "-h";

    public static final String VERSION = "-v";

    public static final String ABOUT = "-a";

    public static final String INFO = "-i";

}
